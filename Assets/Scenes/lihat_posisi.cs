﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static SimpleCloudHandler;

public class lihat_posisi : MonoBehaviour
{


    public RawImage img_location;


    private string nama_lokasi = SimpleCloudHandler.getTargetName();

    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loadImageToUnity());
    }

    public IEnumerator loadImageToUnity()
    {
        Debug.Log("kita lihat lokasinya di :" + nama_lokasi);

        string url = "https://firebasestorage.googleapis.com/v0/b/tugaslast.appspot.com/o/images%2F"+nama_lokasi+".PNG?alt=media";
        Debug.Log("url:" + url);
        WWW w = new WWW(url);
        yield return w;
        Texture2D te = w.texture;
        img_location.texture = te;

    }
}
