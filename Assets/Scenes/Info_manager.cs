﻿using System.Collections.Generic;
using Vuforia;
using System.IO;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using static SimpleCloudHandler;
using Firebase;
using Firebase.Extensions;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.Linq;

public class Info_manager : MonoBehaviour
{
    // DatabaseReference reference;
    public Text txtDeskripsi;
    public Text txtNamaGedung;
    public DatabaseError databaseError;


    private string target_name = SimpleCloudHandler.getTargetName();

    private string namaGedung;
    private string deskripsiGedung;




    void Start()
    {
        // Set these values before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tugaslast.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

        getData();




    }

    public void getData()
    {
        FirebaseDatabase.DefaultInstance.GetReference(target_name).OrderByChild("nama_gedung").ValueChanged += HandleValueChanged;

    }

    void HandleValueChanged(object sender, ValueChangedEventArgs args)
    {

        // // cek error pada database
        if (args.DatabaseError != null)
        {
            Debug.LogError("letak error di :" + args.DatabaseError.Message);
            return;
        }

        var namagedungobject = args.Snapshot.Value as Dictionary<string, System.Object>;

        Debug.Log("test " + namagedungobject);



        foreach (var item in namagedungobject)
        {
            var values = item.Value as Dictionary<string, System.Object>;

            Debug.Log("values");

            int ctr = 0;
            foreach (var v in values)
            {
                ctr++;
                if (ctr == 1)
                {
                    txtDeskripsi.text = v.Value + "\n";
                }
                else if (ctr == 2)
                {
                    txtNamaGedung.text = v.Value + "\n";
                }
            }
        }

    }
}
