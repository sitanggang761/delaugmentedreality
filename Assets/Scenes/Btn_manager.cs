﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Btn_manager : MonoBehaviour
{

    public void loadToCamera(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }

    public void backToCamera(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }



    public void keluar()
    {
        Debug.Log("keluar");
        Application.Quit();
    }



    public void backToHalamanUtama()
    {
        SceneManager.LoadScene("HalamanUtama");
    }

    public void lihatInformasi()
    {
        SceneManager.LoadScene("Informasi");
        // Debug.Log("Ke halaman posisi anda");
    }

    public void toPositionPage(string scenename)
    {
        SceneManager.LoadScene(scenename);
        Debug.Log("Ke halaman posisi anda");
    }

    public void keHalamanBantuan()
    {
        SceneManager.LoadScene("Bantuan");
        // Debug.Log("Ke halaman posisi anda");
    }



    public void keHalamanTentang()
    {
        SceneManager.LoadScene("Tentang");
    }

    public void kembaliKeUtama()
    {
        SceneManager.LoadScene("HalamanUtama");
    }

}
