﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SimpleCloudHandler : MonoBehaviour, IObjectRecoEventHandler
{

    private CloudRecoBehaviour mCloudRecoBehaviour;
    private bool mIsScanning = false;
    
    private ObjectTracker mImageTracker;
    private GameObject newImageTarget;
    // Start is called before the first frame update
    private static string mTargetName = "";

    private ObjectTracker mObjectTracker;
    public ImageTargetBehaviour imageTargetBehaviour;
    public Text txtNamaGedung;

    public GameObject asrama_mamre, asrama_mahanaim, asrama_nazaret,asrama_rusun_4, asrama_kapernaum_silo;

    public GameObject container, kantin_lama, kantin_baru, kantor_administrasi, kantor_satpam;

    public GameObject gedung_4, gedung_5, gedung_7, gedung_8, gedung_9, gedung_rektorat;

    public GameObject koperasi, auditorium, perpustakaan, entrance_hall;

    public GameObject maisonette, studio;
   
    
    //public GameObject gedung_rektorat, entrance_hall, kantin_baru, gedung_9;

    //public GameObject gedung_7, kantor_satpam, studio;




    void Start()
    {


        // register this event handler at the cloud reco behaviour 
        CloudRecoBehaviour cloudRecoBehaviour = GetComponent<CloudRecoBehaviour>();
        

        if (cloudRecoBehaviour)
        {
            cloudRecoBehaviour.RegisterEventHandler(this);
            Debug.Log(cloudRecoBehaviour);
        }

        mCloudRecoBehaviour = cloudRecoBehaviour;



        // grabbing all of the game object;
        //kantin_baru = GameObject.Find("kantin_baru");
        //entrance_hall = GameObject.Find("entrance_hall");
        //gedung_9 = GameObject.Find("gedung_9");
        //gedung_7 = GameObject.Find("gedung_7");
        //kantor_satpam = GameObject.Find("kantor_satpam");
        //studio = GameObject.Find("studio");
        asrama_mamre = GameObject.Find("asrama_mamre");
        asrama_mahanaim = GameObject.Find("asrama_mahanaim");
        asrama_nazaret = GameObject.Find("asrama_nazaret");
        asrama_rusun_4 = GameObject.Find("asrama_rusun_4");
        asrama_kapernaum_silo = GameObject.Find("asrama_kapernaum_silo");
        container =  GameObject.Find("container");
        kantin_lama = GameObject.Find("kantin_lama"); 
        kantin_baru = GameObject.Find("kantin_baru");
        kantor_administrasi = GameObject.Find("kantor_administrasi");
        kantor_satpam = GameObject.Find("kantor_satpam");
        gedung_4 = GameObject.Find("gedung_4");
        gedung_5 = GameObject.Find("gedung_5"); 
        gedung_7 = GameObject.Find("gedung_7");
        gedung_8 = GameObject.Find("gedung_8");
        gedung_9 = GameObject.Find("gedung_9"); 
        gedung_rektorat = GameObject.Find("gedung_rektorat");
        koperasi = GameObject.Find("koperasi");
        auditorium = GameObject.Find("auditorium");
        perpustakaan = GameObject.Find("perpustakaan");
        entrance_hall = GameObject.Find("entrance_hall");
        maisonette = GameObject.Find("maisonette");
        studio = GameObject.Find("studio");



    }

    public void OnInitialized(TargetFinder targetFinder)
    {
        Debug.Log("Cloud Reco initialized");

        // get a reference to the Image Tracker, remember it
        // mImageTracker = (ObjectTracker)TrackerManager.Instance.GetTracker<ObjectTracker>();

    }

    


    public void OnInitError(TargetFinder.InitState initError)
    {
        Debug.Log ("Cloud Reco init error " + initError.ToString());
    }
    public void OnUpdateError(TargetFinder.UpdateState updateError)
    {
        Debug.Log ("Cloud Reco update error " + updateError.ToString());
    }




    public void OnStateChanged(bool scanning)
    {
        mIsScanning = scanning;
        if (scanning)
        {
             // clear all known trackables
            var tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            tracker.GetTargetFinder<ImageTargetFinder>().ClearTrackables(false);
            // tracker.TargetFinder.ClearTrackables(false);
        }
    }



    // Here we handle a cloud target recognition event
    public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
    {
        

            TargetFinder.CloudRecoSearchResult cloudRecoSearchResult =  (TargetFinder.CloudRecoSearchResult)targetSearchResult;
            // do something with the target name
            
            // mTargetName = cloudRecoSearchResult.TargetName;
            mTargetName = targetSearchResult.TargetName;


            
            
            // stop the target finder (i.e. stop scanning the cloud)
            mCloudRecoBehaviour.CloudRecoEnabled = false;

           

            Debug.Log("Name target :"+mTargetName);


        txtNamaGedung.text = mTargetName;
        //statement untuk menampilkan objek 3d berdasarkan  target name

        if (mTargetName == "asrama_mamre")
        {
            asrama_mahanaim.SetActive(false);
            asrama_mamre.SetActive(true);

            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "asrama_mahanaim")
        {
            asrama_mahanaim.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);

        } else if (mTargetName == "asrama_nazaret")
        {
            asrama_nazaret.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);




        } else if (mTargetName == "asrama_rusun_4")
        {
            asrama_rusun_4.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "asrama_silo_kapernaum")
        {
            asrama_kapernaum_silo.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "container")
        {
            container.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "kantin_lama")
        {
            kantin_lama.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "kantin_baru")
        {
            kantin_baru.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "kantor_administrasi")
        {
            kantor_administrasi.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "kantor_satpam") 
        {
            kantor_satpam.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "gedung_4")
        {
            gedung_4.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "gedung_5")
        {
            gedung_5.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "gedung_7")
        {
            gedung_7.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "gedung_8")
        {
            gedung_8.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "gedung_9")
        {
            gedung_9.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "gedung_rektorat")
        {
            gedung_rektorat.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);
        }
        else if (mTargetName == "koperasi")
        {
            koperasi.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);

        }
        else if (mTargetName == "auditorium")
        {
            auditorium.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);

        }
        else if (mTargetName == "perpustakaan")
        {
            perpustakaan.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
            studio.SetActive(false);

        }
        else if (mTargetName == "entrance_hall")
        {
            entrance_hall.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            maisonette.SetActive(false);
            studio.SetActive(false);

        }
        else if (mTargetName == "maisonette")
        {
            maisonette.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); 
            studio.SetActive(false);

        }
        else if (mTargetName == "studio")
        {
            studio.SetActive(true);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);

        }
        else
        {
            studio.SetActive(false);

            asrama_mamre.SetActive(false);
            asrama_mahanaim.SetActive(false);
            asrama_nazaret.SetActive(false);
            asrama_rusun_4.SetActive(false);
            asrama_kapernaum_silo.SetActive(false);
            container.SetActive(false);
            kantin_lama.SetActive(false);
            kantin_baru.SetActive(false);
            kantor_administrasi.SetActive(false);
            kantor_satpam.SetActive(false);
            gedung_4.SetActive(false);
            gedung_5.SetActive(false);
            gedung_7.SetActive(false);
            gedung_8.SetActive(false);
            gedung_9.SetActive(false);
            gedung_rektorat.SetActive(false);
            koperasi.SetActive(false);
            auditorium.SetActive(false);
            perpustakaan.SetActive(false);
            entrance_hall.SetActive(false); ;
            maisonette.SetActive(false);
        }





        if (!mIsScanning)
        {
            // stop the target finder
            mCloudRecoBehaviour.CloudRecoEnabled = true;
            
        }




        if (imageTargetBehaviour)
        {
            // enable the new result with the same ImageTargetBehaviour: 
            ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            // (imageTargetBehaviour)tracker.TargetFinder.EnableTracking(targetSearchResult, imageTargetBehaviour.gameObject);
            tracker.GetTargetFinder<ImageTargetFinder>().EnableTracking(targetSearchResult, imageTargetBehaviour.gameObject);

        }


        


    }
    
    
    //method yang digunakan untuk mengambil target name dari  
    public static string getTargetName()
    {

        return mTargetName;
        

    }
    

   
}
