﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{


	public GameObject asrama_mamre, asrama_mahanaim, asrama_nazaret, asrama_rusun_4, asrama_kapernaum_silo;

	public GameObject container, kantin_lama, kantin_baru, kantor_administrasi, kantor_satpam;

	public GameObject gedung_4, gedung_5, gedung_7, gedung_8, gedung_9, gedung_rektorat;

	public GameObject koperasi, auditorium, perpustakaan, entrance_hall;

	public GameObject maisonette, studio;


	public float rotateSpeed = 50f;
	bool rotateStatus = false;

	public void Rotasi()
	{

		if (rotateStatus == false)
		{
			rotateStatus = true;
		}
		else
		{
			rotateStatus = false;
		}
	}

	void Update()
	{
		if (rotateStatus == true)
		{
			
			asrama_mamre.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			asrama_mahanaim.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			asrama_nazaret.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			asrama_rusun_4.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			asrama_kapernaum_silo.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			container.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			kantin_lama.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			kantin_baru.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			kantor_administrasi.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			kantor_satpam.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			gedung_4.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			gedung_5.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			gedung_7.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			gedung_8.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			gedung_9.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			gedung_rektorat.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			koperasi.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			auditorium.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			perpustakaan.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			entrance_hall.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			maisonette.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
			studio.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
		}
	}

}
